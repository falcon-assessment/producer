package com.falcon.producer;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import static com.falcon.producer.MessageProducer.ROUTING_KEY_JSON;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class MessageProducerTest
{
		private MessageProducer producer;

		private RabbitTemplate template;

		private AmqpAdmin admin;

		private Exchange producerExchange;

		@Before
		public void init()
		{
				admin = mock(AmqpAdmin.class);
				producerExchange = new DirectExchange("producer");
				template = mock(RabbitTemplate.class);
				producer = new MessageProducer(admin, producerExchange, template);
		}

		@Test
		public void produceJsonTest()
		{
				String message = "{\"test\":\"test\"}";
				producer.sendJson(message);
				Mockito.verify(template, times(1))
						.convertAndSend(producerExchange.getName(), ROUTING_KEY_JSON, message);
		}

		@Test
		public void exchangeDeclarationTest()
		{
				verify(admin, times(1)).declareExchange(producerExchange);
		}

}
