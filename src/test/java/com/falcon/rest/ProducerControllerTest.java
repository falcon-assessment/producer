package com.falcon.rest;

import com.falcon.producer.MessageProducer;
import com.falcon.rest.exception.InvalidJsonException;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ProducerControllerTest
{
		private ProducerController controller;

		private MessageProducer producer;

		@Before
		public void init()
		{
				producer = mock(MessageProducer.class);
				controller = new ProducerController(producer);
		}

		@Test(expected = InvalidJsonException.class)
		public void rejectInvalidJsonTest()
		{
				String message = "invalid json";
				controller.postJson(message);
		}

		@Test
		public void acceptJsonObjectTest()
		{
				String message = "{\"test\":\"test\"}";
				controller.postJson(message);
				verify(producer, times(1)).sendJson(message);
		}

		@Test
		public void acceptJsonArrayTest()
		{
				String message = "[{\"test\":\"test\"}]";
				controller.postJson(message);
				verify(producer, times(1)).sendJson(message);
		}

}
