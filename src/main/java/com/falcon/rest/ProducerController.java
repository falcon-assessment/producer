package com.falcon.rest;

import com.falcon.producer.MessageProducer;
import com.falcon.rest.exception.InvalidJsonException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.json.JsonParseException;
import org.springframework.boot.json.JsonParser;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("/produce")
public class ProducerController
{
		private final MessageProducer producer;

		private final JsonParser parser;

		@Autowired
		public ProducerController(MessageProducer producer)
		{
				this.producer = producer;
				this.parser = new JacksonJsonParser();
		}

		@PostMapping(value = "/json", consumes = MediaType.APPLICATION_JSON_VALUE)
		public void postJson(@RequestBody String message)
		{
				validate(message);
				producer.sendJson(message);
		}

		private void validate(String message)
		{
				try
				{
						parser.parseMap(message);
				}
				catch (JsonParseException e)
				{
						try
						{
								parser.parseList(message);
						}
						catch (JsonParseException ex)
						{
								throw new InvalidJsonException("Invalid json");
						}
				}
		}
}
