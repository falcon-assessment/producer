package com.falcon.config;

import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessagingConfig
{
		private final String exchangeName;

		private final String exchangeType;

		@Autowired
		public MessagingConfig(@Value("${spring.rabbitmq.exchanges.producer.name}") String exchangeName,
		                       @Value("${spring.rabbitmq.exchanges.producer.type}") String exchangeType)
		{
				this.exchangeName = exchangeName;
				this.exchangeType = exchangeType;
		}

		@Bean
		public Exchange producerExchange()
		{
				return new ExchangeBuilder(exchangeName, exchangeType).build();
		}
}
