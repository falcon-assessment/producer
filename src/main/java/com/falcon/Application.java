package com.falcon;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableAutoConfiguration
@EnableSwagger2
public class Application
{
		public static void main(String[] args)
		{
				new SpringApplicationBuilder()
						.sources(Application.class)
						.properties("spring.config.name=application,messaging")
						.bannerMode(Banner.Mode.OFF)
						.run(args);
		}
}
