package com.falcon.producer;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageProducer
{
		public static final String ROUTING_KEY_JSON = "json";

		private final RabbitTemplate template;

		private final Exchange producerExchange;

		@Autowired
		public MessageProducer(AmqpAdmin admin, Exchange producerExchange, RabbitTemplate template)
		{
				this.template = template;
				this.producerExchange = producerExchange;
				admin.declareExchange(producerExchange);
		}

		public void sendJson(String message)
		{
				template.convertAndSend(producerExchange.getName(), ROUTING_KEY_JSON, message);
		}
}
