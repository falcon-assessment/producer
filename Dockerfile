FROM openjdk:12-alpine

WORKDIR /app

COPY target/producer.jar .

ENTRYPOINT exec java $JAVA_OPTS -jar producer.jar